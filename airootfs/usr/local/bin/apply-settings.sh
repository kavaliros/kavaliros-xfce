sed -i s/Adwaita/Bibata-Modern-Amber/g /usr/share/icons/default/index.theme

if [ -f /usr/bin/brsaneconfig4 ]; then
    brsaneconfig4 -a name=Brother model=j4610dw ip=192.168.178.17
    systemctl enable cups
fi

#Clementine
if [ -f /usr/bin/clementine ]; then
    tar xvf /etc/calamares/appsettings/Clementine.tar.gz -C /home/$1/.config
fi

# VirtualBox
if [ -f /usr/bin/virtualbox ]; then
    gpasswd -a $1 vboxusers
fi

if [ -f /usr/lib/systemd/system/vboxservice.service ]; then
    gpasswd -a $1 vboxsf
    systemctl enable vboxservice
fi

# SyncThing
if [ -f usr/bin/syncthing ]; then
    systemctl enable syncthing@$1
fi

# NordVPN
if [ -f /usr/bin/nordvpn ]; then
    systemctl enable nordvpnd
    gpasswd -a $1 nordvpn
    nordvpn whitelist add subnet 192.168.0.0/16
fi

# ICAclient. 
if [ -d /opt/Citrix/ICAClient ]; then
    tar xvf /etc/calamares/appsettings/ICAClient.tar.gz -C /home/$1
fi

# NetExtender
if [ -f /usr/sbin/pppd ]; then
    chmod u+s /usr/sbin/pppd
    tar xvf /etc/calamares/appsettings/netextender.tar.gz -C /home/$1
fi

#Dotnet
if [ -d /usr/share/dotnet ]; then
    dotnet tool install --global dotnet-dev-certs
    dotnet tool install --global dotnet-ef
    sed -i s/username/$1/g /etc/profile.d/dotnet.sh
    dotnet dev-certs https
fi

#Visual Studio Code OSS
if [ -f /usr/bin/code-oss ]; then
    tar xvf /etc/calamares/appsettings/vscode-oss.tar.gz -C /home/$1
    tar xvf /etc/calamares/appsettings/Code\ -\ OSS.tar.gz -C /home/$1/.config
fi

#Visual Studio Code 
if [ -f /opt/visual-studio-code/bin/code ]; then
    tar xvf /etc/calamares/appsettings/vscode.tar.gz -C /home/$1
    tar xvf /etc/calamares/appsettings/Code.tar.gz -C /home/$1/.config
fi

#Libre Office
if [ -f /usr/bin/libreoffice ]; then
    tar xvf /etc/calamares/appsettings/libreoffice.tar.gz -C /home/$1/.config
fi

#Calibre
if [ -f /usr/bin/calibre ]; then
    tar xvf /etc/calamares/appsettings/calibre.tar.gz -C /home/$1/.config
fi

#Gscan2Pdf
if [ -f /usr/bin/gscan2pdf ]; then
    tar xvf /etc/calamares/appsettings/gscan2pdfrc.tar.gz -C /home/$1/.config
fi

# Remove settings
rm -Rf /etc/calamares/appsettings
