#!/bin/sh
pacman-key --init
pacman-key --populate archlinux
reflector --protocol https --latest 10 --sort rate --country nl --save /etc/pacman.d/mirrorlist
pacman-key --keyserver hkps://keyserver.ubuntu.com --recv-key 76C6E477042BFE985CC220BD9C08A255442FAFF0
pacman-key --lsign 76C6E477042BFE985CC220BD9C08A255442FAFF0