echo "Finalize Applications"

# Remove Calamares icon form all desktops
if [ -f /home/$1/Desktop/calamares.desktop ]; then
    echo "Remove Calamares icon from desktop"
    rm /home/$1/Desktop/calamares.desktop
fi

#Enable kernel update
echo "Enable efi-update service"
systemctl enable efi-update.path

#Enable NTP
echo "Enable NTP service"
timedatectl set-ntp true

#Update user ID for same id as synaology nas
echo "Change id of user $1"
usermod -u 1026 $1

# Boot EFI
echo "Copy boot files"
cp /boot/vmlinuz-linux-lts /boot/efi/KavalirOS/
cp /boot/initramfs-linux-lts.img /boot/efi/KavalirOS/
cp /boot/amd-ucode.img /boot/efi/KavalirOS/

#Micro code
echo "Enable microcode in bootloader"
sed -i '/^[[:blank:]]*#/d' /boot/efi/loader/entries/KavalirOS.conf
sed -i '/^[[:space:]]*$/d' /boot/efi/loader/entries/KavalirOS.conf
sed -i '2 a initrd /KavalirOS/amd-ucode.img' /boot/efi/loader/entries/KavalirOS.conf

#Pacman
echo "Fix pacman config for repository on nas"
sed -i 's/data\/repo/data\/nas\/roelof\/KavalirOS\/Repo/g' /etc/pacman.conf
sed -i  "$ a Defaults:$1 timestamp_timeout=30" /etc/sudoers
pacman-key --keyserver hkps://keyserver.ubuntu.com --recv-key 76C6E477042BFE985CC220BD9C08A255442FAFF0
pacman-key --lsign 76C6E477042BFE985CC220BD9C08A255442FAFF0

# Disable obsolete services
systemctl disable livecd-alsa-unmuter
systemctl disable reflector
systemctl disable pacman-init.service

#Create mirrorlist NL
reflector --protocol https --latest 10 --sort rate --country nl --save /etc/pacman.d/mirrorlist

#Hosts
sed -i '3 a 192.168.178.40  synology storage' /etc/hosts
sed -i '4 a 192.168.178.44  almabox sqlserver' /etc/hosts
